const express = require('express');
const path = require('path');
const fs = require('fs');
const terms = fs.readFileSync(path.join(__dirname, 'terms.html'), { encoding : 'utf8' });

const app = express();

app.get('/style', (req, res, next)=>{
    return res.contentType('text/css').sendFile(path.join(__dirname, 'style.css'))
});

app.get('/terms/:language', (req, res, next) => {
    return res.send(terms.replace('{{language}}', req.params.language));
});


app.listen(3030, ()=>console.log('listening on port 3030'))